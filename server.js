'use strict';
//initiate express js
process.env.TZ = 'WIT';

var express = require('express');
var app = express();

var path = require('path');

var clc = require('cli-color');

global.appRoot = path.resolve(__dirname);

app.use(function (req, res, next) {
	global.siteUrl = req.protocol + '://' + req.get('host');
	return next();
});
//devstart
// process.env.NODE_ENV = (process.env.NODE_ENV!=null)?process.env.NODE_ENV:'production';
//devstart
process.env.NODE_ENV = (process.env.NODE_ENV!=null)?process.env.NODE_ENV:'development';

if (process.env.NODE_ENV == 'production')
	console.log(clc.red.bgWhite.underline('You are now in production mode of roomme, every transaction are considered as REAL trasaction!'));
else if (process.env.NODE_ENV == 'development')
	console.log(clc.black.bgWhite.underline('have fun while in cloud development mode!'));
else
	console.log(clc.black.bgWhite.underline('have fun while in local development mode!'));

require('./app/config'+((process.env.NODE_ENV==='local-dev')?'/local-dev':''));

// Start the server

//session modules
require('./app/kernel/session')(app);

//security modules
require('./app/kernel/security')(app);

//auth modules
var passport = require('passport');

require('./app/kernel/passport')(passport, app);

//http
require('./app/kernel/http')(app);

require('./app/kernel/validator')(app);

//templating
require('./app/kernel/render')(app, path, express);

// require('./app/kernel/cron')();

//ctrl and middleware
var ctrl = require('./app/controller');
var mdlwr = require('./app/middleware');

var router = express.Router();
app.use(router);

require('./app/kernel/interceptor')(app);

//helpers
var uploader = require('./app/helper/uploader');
var viewhelper = require("./app/helper/view")(app);

var appRoutes = new Object;

require('./app/kernel/routing')(app, express, router, appRoutes);

require('./app/routes')(appRoutes, ctrl, passport, router, mdlwr, uploader);

if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'development')
	var port = process.env.PORT || 3000;
else
	var port = global.APP_CONFIGS.app.port;

app.listen(port);
console.log('API listening at port ' + port);
