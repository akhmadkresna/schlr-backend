module.exports = function(app) {

	var ejs = require('ejs');

	var viewLocation = __dirname+'/../../resource/view/helper/';
	var fs = require('fs');

	var numeral = require('numeral');

	app.locals.currency = function(amount, currency) 
	{
		return currency+' '+numeral(amount).format('0,0');
	}

	app.locals.rating = function(total) 
	{
		if (total == null) 
			return "Not rated yet";

		var final = "";

		for (var i = total - 1; i >= 0; i--) 
		{
			final += '<i class="fa fa-star gold-color" aria-hidden="true"></i>';	
		}

		return final;
	}

	app.locals.paginator = function(totalPage, currentPage, url, parameter) {
		var thepaginator = '';

		url += '?';

		var addparams = '';

		if (parameter.length > 0)
		{
			for (var i = parameter.length - 1; i >= 0; i--)
			{
				addparams += '&'+parameter[i].key+'='+parameter[i].value;
			}
		}

		currentPage = parseInt(currentPage);

		var pagerOpen = '<nav><ul class="pagination">';

		if (currentPage === 1)
		{
			var prevNav = '<li class="disabled">\
			<span aria-hidden="true">&laquo;</span>\
			</a>\
			</li>';
		}
		else
		{
			var prevNav = '<li>\
			<a href="/'+url+'page='+(currentPage-1)+'" aria-label="Previous">\
			<span aria-hidden="true">&laquo;</span>\
			</a>\
			</li>';
		}

		if (currentPage === totalPage)
		{
			var nextNav = '<li class="disabled">\
			<span aria-hidden="true">&raquo;</span>\
			</a>\
			</li>';
		}
		else
		{
			var nextNav = '<li>\
			<a href="/'+url+'page='+(currentPage+1)+'" aria-label="Next">\
			<span aria-hidden="true">&raquo;</span>\
			</a>\
			</li>';
		}

		var pagerClose = '</ul></nav>';
		var pages = '';

		if (totalPage > 10)
		{
			if (currentPage <= 7)
			{
				for (var i = 1; i <= 8; i++) {
					pages += '<li '+(i === currentPage?'class="active"':'')+'><a href="/'+url+'page='+i+addparams+'">'+i+'</a></li>';
				}

				pages += '<li class="disabled"><span>...</span></li>';
				pages += '<li><a href="/'+url+'page='+(totalPage-1)+'">'+(totalPage-1)+'</a></li>';
				pages += '<li><a href="/'+url+'page='+totalPage+'">'+totalPage+'</a></li>';
			}
			else if (currentPage >7 && (currentPage < totalPage-7))
			{
				pages += '<li><a href="/'+url+'page=1">1</a></li>';
				pages += '<li><a href="/'+url+'page=2">2</a></li>';
				pages += '<li class="disabled"><span>...</span></li>';

				for (var i = currentPage-2; i <= (currentPage+2); i++)
					pages += '<li '+(i === currentPage?'class="active"':'')+'><a href="/'+url+'page='+i+addparams+'">'+i+'</a></li>';

				pages += '<li class="disabled"><span>...</span></li>';
				pages += '<li><a href="/'+url+'page='+(totalPage-1)+'">'+(totalPage-1)+'</a></li>';
				pages += '<li><a href="/'+url+'page='+totalPage+'">'+totalPage+'</a></li>';
			}
			else if (currentPage >7)
			{
				for (var i = 1; i <= 8; i++)
					pages += '<li '+(i === currentPage?'class="active"':'')+'><a href="/'+url+'page='+i+addparams+'">'+i+'</a></li>';

				pages += '<li class="disabled"><span>...</span></li>';
				pages += '<li><a href="/'+url+'page='+(totalPage-1)+'">'+(totalPage-1)+'</a></li>';
				pages += '<li><a href="/'+url+'page='+totalPage+'">'+totalPage+'</a></li>';

			}
			else
			{
				pages += '<li><a href="/'+url+'page=1">1</a></li>';
				pages += '<li><a href="/'+url+'page=2">2</a></li>';
				pages += '<li class="disabled"><span>...</span></li>';

				for (var i = totalPage-7; i <= totalPage; i++)
					pages += '<li '+(i === currentPage?'class="active"':'')+'><a href="/'+url+'page='+i+addparams+'">'+i+'</a></li>';

			}
		}
		else
		{
			for (var i = 1; i <= totalPage; i++)
				pages += '<li '+(i === currentPage?'class="active"':'')+'><a href="/'+url+'page='+i+addparams+'">'+i+'</a></li>'

		}
		thepaginator = pagerOpen+prevNav+pages+nextNav+pagerClose;
		return thepaginator;
	}

	app.locals.errorAlert = function() {
		if (app.locals.alerts.error != null)
		{
			var template = fs.readFileSync(viewLocation+'errorAlert.ejs', 'utf8'); 	
			return ejs.render(template, {message: app.locals.alerts.error});
		}
	}

	app.locals.successAlert = function() {
		if (app.locals.alerts.success != null)
		{
			var template = fs.readFileSync(viewLocation+'successAlert.ejs', 'utf8'); 	
			return ejs.render(template, {message: app.locals.alerts.success});
		}
	}

	app.locals.postBody = function(name) {
		if (app.locals.body != null) 
			return (app.locals.body[name] == null || typeof app.locals.body[name] == 'undefined')?'':app.locals.body[name];
	}

	app.locals.formError = function(name) {
		if (app.locals.errMessage != null) 
		{
			for (var i = app.locals.errMessage.length - 1; i >= 0; i--) 
			{
				if (app.locals.errMessage[i].param == name) 
					return app.locals.errMessage[i].msg
			}
			return null
		}
	}

	app.locals.queryParam = function(name) {
		return app.locals.query[name];
	}

	app.locals.navigator = function() {
		var template = fs.readFileSync(viewLocation+'navigator.ejs', 'utf8'); 	

		return ejs.render(template, {user: app.locals.user});
	}

	app.locals.notification = function() {
		if (app.locals.user) 
		{
			var template = fs.readFileSync(viewLocation+'notification.ejs', 'utf8'); 	

			return ejs.render(template, {user: app.locals.user, notif: app.locals.notifs});
		}
	}

	app.locals.formatedDate = function(dateObjt) 
	{
		var year = dateObjt.getFullYear();
		var month = dateObjt.getMonth();
		var day = dateObjt.getDate();
		var hour = dateObjt.getHours();
		var minute = dateObjt.getMinutes();

		var monthList = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
		var formated = day+" "+monthList[month]+" "+year+', '+hour+':'+minute;
		return formated;
	}

	app.locals.formatedDateOnly = function(dateObjt) 
	{
		var year = dateObjt.getFullYear();
		var month = dateObjt.getMonth();
		var day = dateObjt.getDate();
		var hour = dateObjt.getHours();
		var minute = dateObjt.getMinutes();

		var monthList = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
		var formated = day+" "+monthList[month]+" "+year;
		return formated;
	}
};
