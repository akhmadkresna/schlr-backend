exports.pushIOS = function(token, alert, opt) {
	var apn  = require("apn");

	if (process.env.NODE_ENV === 'production') {
		var apnOpt = {
			"cert": __dirname+"/./../../resource/certificates/apn/development/roomme-prod.pem",
			"key": __dirname+"/./../../resource/certificates/apn/development/roomme-prod.pem",
			"production" : true
		};
	}
	else {
		var apnOpt = {
			"cert": __dirname+"/./../../resource/certificates/apn/development/roomme-dev.pem",
			"key": __dirname+"/./../../resource/certificates/apn/development/roomme-dev.pem",
			"production" : false
		};
	}


	var apnProvider = new apn.Provider(apnOpt);

	var note = new apn.Notification();

	note.expiry = Math.floor(Date.now() / 1000) + 3600;
	note.badge = opt.badge ? opt.badge : 1;
	note.alert = alert;
	note.topic = "com.roomme";

	apnProvider.send(note, token).then( (result) => {
		console.log(result.failed[0].response)
	});
}

exports.pushGCM = function(token, alert, opt) {
	// const gcmKey = global.APP_CONFIGS.gcm.client[0].api_key.current_key;
	const gcmKey = global.APP_CONFIGS.gcm.key;

	var gcm = require('node-gcm');

	var sender = new gcm.Sender(gcmKey);

	var message = new gcm.Message({
		priority: 'high',
		notification: {
			title: 'Roomme',
			body: alert
		}
	});

	var registrationTokens = [];

	registrationTokens.push(token);

	sender.send(message, { registrationTokens: registrationTokens }, 10, function (err, response) {
		if(err) 
		{
			console.error(err);
		}
		else    console.log(response);
	});
}