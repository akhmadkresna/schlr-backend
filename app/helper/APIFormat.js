'use strict';
module.exports ={
		response(message, data, error) {
			return {
				message: message,
				result: data, 
				error: (error!=null)?error:null	
			};
		},
		responsePaging(message, data, error) {
			var result = {
				items: data.values,
				countperpage : parseInt(data.countPerPage),
				page : parseInt(data.currentPage),
				totalPage: Math.ceil(parseInt(data.totalData)/parseInt(data.countPerPage))
			}

			return {
				message: message,
				result: result, 
				error: (error!=null)?error:null
			};
		},

		/*
		@param 
			data = {
				total 			: <total unfiltered row in database table>,
				totalFiltered	: <total filtered row in database table>,
				filteredData 	: <filtered data array to be shown in a datatable page>
			}
		*/
		responseDataTable(draw, data, error){
			var totalFiltered = (data.totalFiltered>data.total)?data.total:data.totalFiltered;

			return {
				draw: (draw)?draw:0,
				recordsTotal: data.total,
				recordsFiltered: totalFiltered,
				data: data.filteredData
			};
		}

}