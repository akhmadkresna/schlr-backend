'use strict';

module.exports = function(appRoutes, ctrl, passport, router, mdlwr, uploader) {

	//routes for cms
	appRoutes.cms.get('/landing', ctrl.CMS.Landing.index);
	appRoutes.cms.get('/login', ctrl.CMS.Auth.show);
	appRoutes.cms.post('/signup', ctrl.CMS.Account.signup);
	appRoutes.cms.post('/login', passport.authenticate('local-login', {
        successRedirect : '/partner', // redirect to the secure profile section
        failureRedirect : '/partner/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));
	appRoutes.cms.get('/logout', mdlwr.Auth.isAuthed, ctrl.CMS.Auth.destroy);
	appRoutes.cms.post('/update', mdlwr.Auth.isAuthed, ctrl.CMS.Account.update);

	appRoutes.cms.get('/', mdlwr.Auth.isAuthed, ctrl.CMS.Dashboard.index);
	appRoutes.cms.get('/candidate', mdlwr.Auth.isAuthed, ctrl.CMS.Candidate.index);
	appRoutes.cms.get('/candidate/:id/detail', mdlwr.Auth.isAuthed, ctrl.CMS.Candidate.show);
	appRoutes.cms.get('/candidate/:id/profile', mdlwr.Auth.isAuthed, ctrl.CMS.Candidate.profile);
	
	appRoutes.cms.post('/listings', mdlwr.Auth.isAuthed, uploader.listingimage.single('image'), ctrl.CMS.Listing.image, ctrl.CMS.Listing.store);
	appRoutes.cms.get('/listings', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.index);
	appRoutes.cms.get('/listings/:id/detail', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.show);
	appRoutes.cms.get('/faculty/:id/detail', mdlwr.Auth.isAuthed, ctrl.CMS.Faculty.show);
	appRoutes.cms.get('/listings/:id/edit', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.edit);
	appRoutes.cms.post('/listings/:id', mdlwr.Auth.isAuthed, uploader.listingimage.single('image'), ctrl.CMS.Listing.image, ctrl.CMS.Listing.update);
	appRoutes.cms.get('/listings/:id/delete', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.delete);
	appRoutes.cms.get('/listings/create', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.create);
	appRoutes.cms.post('/program/store', mdlwr.Auth.isAuthed, ctrl.CMS.Program.store);
	appRoutes.cms.get('/program/:id/delete', mdlwr.Auth.isAuthed, ctrl.CMS.Program.delete);
	appRoutes.cms.post('/program/:id/update', mdlwr.Auth.isAuthed, ctrl.CMS.Program.update);
	appRoutes.cms.post('/faculty/:id/update', mdlwr.Auth.isAuthed, ctrl.CMS.Faculty.update);
	appRoutes.cms.get('/faculty/:id/edit', mdlwr.Auth.isAuthed, ctrl.CMS.Faculty.show);
	appRoutes.cms.get('/faculty/:id/delete', mdlwr.Auth.isAuthed, ctrl.CMS.Faculty.delete);
	appRoutes.cms.get('/listings/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.show);
	appRoutes.cms.get('/profile', mdlwr.Auth.isAuthed, ctrl.CMS.Account.index);
	appRoutes.cms.get('/faculty', mdlwr.Auth.isAuthed, ctrl.CMS.Account.faculty);
	appRoutes.cms.post('/setupfaculty', mdlwr.Auth.isAuthed, ctrl.CMS.Account.setupfaculty, ctrl.CMS.Account.createfaculty);

	// //end of route for cms
	//routes for web
	appRoutes.open.get('/', ctrl.WEB.Home.index);
	// appRoutes.open.post('/register', ctrl.CMS.register);
	appRoutes.open.get('/terms-and-condition', ctrl.WEB.Home.tnc);
	//end of routes for web
};
