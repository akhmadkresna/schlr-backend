'use strict';
var jwt = require('jsonwebtoken');

exports.isAuthed = function(req, res, next) 
{
    req.session['visitURL'] = req.protocol + '://' + req.get('host') + req.originalUrl;
    
    if (req.isAuthenticated())
        return next();

    return res.redirect('/partner/login');
};

//check if user is not logged in, redirect to desired url if not
exports.isGuest = function(req, res, next) 
{
    if (!req.isAuthenticated())
        return next();
    
    return res.redirect('/partner');
};


