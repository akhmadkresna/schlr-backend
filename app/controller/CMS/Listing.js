'use strict';
var request = require('request');
var querystring = require('querystring');
var fs = require('fs');
var FormData = require('form-data');
var requestp = require('request-promise');

exports.image = function(req, res, next) 
{	
	if (req.file == undefined) {
		console.log("BIG NOOOOOOOO")
		return next();
	} 		
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
		console.log("EDIT IMAGE :",req.session.image_id_edit);
		if (req.session.image_id_edit) {
			console.log("===================== IMAGE UPDATE ========================")
	    var formData = {
		    user_id: req.user.user_id,
		    file: req.file.buffer,
		    type: '6',
		    id: req.session.image_id_edit
			};
			var url = 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/images/update'
		} else {
			console.log("===================== IMAGE CREATE ========================")
			var formData = {
			    user_id: req.user.user_id,
			    file: req.file.buffer,
			    type: '6'
				};
			var url = 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/images/create'

		}

	var headers = {
		'Authorization' : auth,
      	'Content-Type': 'application/x-www-form-urlencoded'      
    }
    var options = {
            url: url,
            method: 'POST',
            headers: headers,
            formData: formData,
        } 

	request(options, function(err, response, body) {
	  	if (err) {
	    	return res.status(500).json({'error' : err});
	  	}
	  	if (typeof(body) == 'string') {
	  		body = JSON.parse(body);;
	  	}
	  	if (body){
	  		if (typeof(body.id) == 'string'){
	  			body.id = parseInt(body.id);
	  		}
	  		req.listing_image_id = body.id
			return next();
		}
		return res.status(500).json({'error' : body});

	});
    
   
};

exports.index = function(req, res)
{		
		var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
		var headers = {
			'Authorization' : auth,
			'Content-Type': 'application/json',
				
		}
		var options = {
						url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/scholarships/showfilterbyprovider',
						method: 'POST',
						headers: headers,
						// json: {'provider_id': 'Org-62-1503163043.67157'}
						json: {'provider_id': req.user.user_id}

				} 
		
		request(options, function (error, response, body) {
				console.log("SCHOLARSIPS ::",body)
				if (body && body.success) {
					req.session.listings = body.scholarship;
					return res.render('CMS/listing/index', {
						layout: 'CMS/layout/master',
						message: req.flash('listMessage'),
						listings: req.session.listings ,
						keyword: '',
						status: '',
						currentpage: (req.query.page === undefined?1:req.query.page),
						totalPage: Math.ceil(parseInt(req.session.listings.count)/20),
					});
				}
				return res.status(404).json({'error' : error});
		}).on('error', function(err) {
				return res.status(500).json({'error' : err});
		});
		
	

};

exports.show = function(req, res)
{
	req.assert('id', 'Invalid listing').isInt({min:1});
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
	var headers = {
		'Authorization' : auth,
		'Content-Type': 'application/json',
			
	}
	var options = {
		url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/providers/showcandidatescholarship',
		method: 'POST',
		headers: headers,
		json: {'provider_id': req.user.user_id,
						'scholarship_id' : req.params.id
					}

	} 
	var errors = req.validationErrors();	
	var listing = get_listing_byid(req.session.listings , req.params.id)
	if (errors.length>0)
		return res.status(400).json({'error' : errors});
	request(options, function (error, response, body) {
		console.log("detail API :",listing)

		if (body && body.success) {
			var candidates = []
			// for (var i=0;i<body.candidate_scholarship.length;i++){
			// 	console.log("STIDENT ::",body.candidate_scholarship[i].student_id)
			// 	candidates.push(body.candidate_scholarship[i].student_id)
			// }
			return res.render('CMS/listing/show', {
				layout: 'CMS/layout/master',
				listing: listing,
				// candidate: candidates
			});
		
		}
		return res.status(404).json({'error' : error});
		})
}
exports.create = function(req, res)
{
	return res.render('CMS/listing/create', {
		layout: 'CMS/layout/master'
	});
};

function get_listing_byid (list, id){
	for (var i = 0;i<list.length;i++){
		id = parseInt(id)
		if (list[i].id == id){
			return list[i]
		}
	}
}
exports.edit = function(req, res)
{	
	var listing = get_listing_byid(req.session.listings , req.params.id)
	if (listing.image_id) {
		req.session.image_id_edit = listing.image_id.id;
	}
	console.log("SCHOLAR EDIT CHECK IMAGE ::::::::::",req.session.image_id_edit)
	return res.render('CMS/listing/edit', {
		layout: 'CMS/layout/master',
		listing: listing
	});
}

exports.update = function(req, res)
{
	req.assert('title', 'Title is empty').notEmpty();
  req.assert('start_date', 'Invalid start date').notEmpty();
  req.assert('end_date', 'Invalid end date').notEmpty();
  req.assert('ckedit', 'Description is empty').notEmpty();
  req.assert('quota', 'Quota is empty').notEmpty();
  var errors = req.validationErrors();

  if (errors.length>0){
  	return res.status(400).json({'error-form' : errors});
  }
  // if(req.session.image_id_edit == undefined){
  // 	req.session.image_id_edit = req.listing_image_id;
  // }
  if (req.listing_image_id == undefined ){
  	if (req.session.image_id_edit == undefined) {
  		return res.status(400).json({'image-error' : 'image not created'});
  	} else {
  		req.listing_image_id = req.session.image_id_edit
  	}
  	
  }
  console.log("have image ?",req.session.image_id_edit)
  var scholar = {
  "id" : req.params.id,
  "provider_id": req.user.user_id,
  "date_start": req.body.start_date,
	"date_experied": req.body.end_date,
  "scholarship_type": "2",
  "status":"0",
  "title": req.body.title,
  "quota": req.body.quota,
  "descripe": req.body.ckedit,
  "country_id": "2",
  "grade_id": "2",
  "image_id": req.listing_image_id,
  "city_id": "2",
  "minimum_grade": "2",
  "faculty_id": "2",
  "program_id": "2",
  "type_country": "0",
  "scholarshippoint_id": "1"
  
}
console.log(" UPDATE IMAGE LAST ID :", req.session.image_id_edit)
var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
var options = {
		method: 'POST',
		url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/scholarships/update',
		headers : {
         	"Authorization" : auth,
					'Content-Type': 'application/json',
        },
		json: scholar,
		
};
request(options, function (error, response, body) {
	console.log('checkk edit', body)	
	  if (body && body.success) {
	  	req.flash('listMessage', 'Scholarship edited')
	  	return res.redirect('/partner/listings');
	  }
	  req.flash('listMessage', response['message']);
	  return res.redirect('/partner/listings');
	  // return res.status(404).json({'error-update' : error , 'response' : body});
	})

}



exports.delete = function(req, res, next){
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");

	var options = {
			method: 'DELETE',
			url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/scholarships/'+req.params.id,
			headers : {
	         	"Authorization" : auth,
						'Content-Type': 'application/json',
	        },
			
	};
	request(options, function (error, response, body) {
        if (typeof(body) == 'string') {
		  		body = JSON.parse(body);;
		  	}
        if (body && body.success) {
        	req.flash('listMessage', 'Scholarship : deleted')
            return res.redirect('/partner/listings');
        }
        return res.status(404).json({'error' : 'Not found'});
    })

}
exports.store = function(req, res, next)
{
    req.assert('title', 'Title is empty').notEmpty();
    req.assert('start_date', 'Invalid start date').notEmpty();
    req.assert('end_date', 'Invalid end date').notEmpty();
    req.assert('ckedit', 'Description is empty').notEmpty();
    req.assert('quota', 'Quota is empty').notEmpty();
    var errors = req.validationErrors();

    if (errors.length>0){
    	return res.status(400).json({'error' : errors});
    }
    var scholar = {
	  "provider_id": req.user.user_id,
	  "date_start": req.body.start_date.replace('/','').replace('/',''),
		"date_experied": req.body.end_date.replace('/','').replace('/',''),
	  "scholarship_type": "2",
	  "status":"0",
	  "title": req.body.title,
	  "quota": req.body.quota,
	  "descripe": req.body.ckedit,
	  "country_id": "2",
	  "grade_id": "2",
	  "image_id": req.listing_image_id,
	  "city_id": "2",
	  "minimum_grade": "2",
	  "faculty_id": "2",
	  "program_id": "2",
	  "type_country": "0",
	  "scholarshippoint_id": "1"
	  
	}
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
	var options = {
			method: 'POST',
			url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/scholarships/create',
			headers : {
	         	"Authorization" : auth,
						'Content-Type': 'application/json',
	        },
			json: scholar,
			
	};
	request(options, function (error, response, body) {
        if (body && body.success) {
        	req.flash('listMessage', 'Scholarship created')
            return res.redirect('/partner/listings');
        }
        return res.status(404).json({'error' : 'Not found'});
    })

    
};
