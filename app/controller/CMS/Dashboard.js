'use strict';
var request = require('request-promise');
// var async = require('async');
var Promise = require('bluebird');

exports.index = function(req, res)
{
	
	// create request objects
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
	var headers = {
		'Authorization' : auth,
		'Content-Type': 'application/json',
			
	}
	var requests = [{
		url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/providers/shownumberactivescholarship',
		method: 'POST',
		headers: headers,
		json: {'provider_id': req.user.user_id}
	}, {
		url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/providers/shownumberactiveapplier',
		method: 'POST',
		headers: headers,
		json: {'provider_id': req.user.user_id}
	}];
	
	Promise.map(requests, function(obj) {
		return request(obj).then(function(body) {
			return body;
		});
	}).then(function(results) {
		console.log("DOUBLE CALL :",results);
		var applier = 0
		var scholarships = 0
		for (var i = 0; i < results.length; i++) {
			if (results[i].success && results[i].hasOwnProperty('applier_count')){
				applier = results[i].applier_count
			}
			if (results[i].success && results[i].hasOwnProperty('scholarship_count')){
				scholarships = results[i].scholarship_count
			}
		}
		return res.render('CMS/index', {
			layout: 'CMS/layout/master',
			applier: applier,
			scholarships: scholarships,
		});
	}, function(err) {
		return res.status(500).json({'error' : err});
	});
}
// exports.index = function(req, res) 
// {
// 	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
// 	var headers = {
// 		'Authorization' : auth,
// 		'Content-Type': 'application/json',
			
// 	}
// 	var options = {
// 					url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/providers/shownumberactivescholarship',
// 					method: 'POST',
// 					headers: headers,
// 					// json: {'provider_id': 'Org-62-1503163043.67157'}
// 					json: {'provider_id': req.user.user_id}
// 	} 
// 	return request(options, function (error, response, body) {
// 		console.log("BODY")
	

// 		if (body && body.success) {
		
// 		}
// 		return res.status(404).json({'error' : error});
// 	}).then( function(err) {

// 		var options2 = {
// 			url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/providers/shownumberactiveapplier',
// 			method: 'POST',
// 			headers: headers,
// 			// json: {'provider_id': 'Org-62-1503163043.67157'}
// 			json: {'provider_id': req.user.user_id}
// 		}
// 			return res.status(500).json({'error' : err});
// 	}).then( function() {
		// return res.render('CMS/index', {
		// 	layout: 'CMS/layout/master'
		// });
// 	});

// };