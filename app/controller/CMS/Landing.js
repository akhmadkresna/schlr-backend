'use strict';

exports.index = function(req, res)
{
	return res.render('CMS/landing', {
		layout: 'WEB/layout/landing',
		status: null,
		// googlemeta: (process.env.NODE_ENV == 'production')?'<meta name="google-site-verification" content="gTFox6d_5Yig1BT9pPqW-aW4aaUohd9Tuda-1wTemNI" />':''
	});
};

exports.tnc = function(req, res)
{
	return res.render('WEB/tnc', {
		layout: 'CSM/layout/master'
	});
}
