'use strict';
var request = require('request');
var querystring = require('querystring');
var fs = require('fs');
var FormData = require('form-data');


exports.index = function(req, res)
{		
		var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
		var headers = {
			'Authorization' : auth,
      'Content-Type': 'application/json',
            
    }
  var options = {
          url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/providers/showcandidate',
          method: 'POST',
          headers: headers,
          // json: {'provider_id': 'Org-62-1503163043.67157'}
          json: {'provider_id': req.user.user_id}

      } 
  
  request(options, function (error, response, body) {
		if (body && body.success) {
			req.session.listings = body.candidate;
				return res.render('CMS/candidate/index', {
				layout: 'CMS/layout/master',
				message: req.flash('listMessage'),
				listings: req.session.listings ,
				keyword: '',
				status: '',
				currentpage: (req.query.page === undefined?1:req.query.page),
				totalPage: Math.ceil(parseInt(req.session.listings.count)/20),
			});
		}
		return res.status(404).json({'error' : 'Not found'});
  }).on('error', function(err) {
		return res.status(500).json({'error' : err});
  });
		
	

};

exports.show = function(req, res)
{
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
	var headers = {
		'Authorization' : auth,
			'Content-Type': 'application/json',
			
	}
	var options = {
					url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/facultys/showonefaculty',
					method: 'POST',
					headers: headers,
					json: {
						'provider_id': req.user.user_id,
						'id': req.params.id
					}

			} 
	
	request(options, function (error, response, body) {
		console.log("student stuff ::",body)
		if (body && body.success) {
			req.session.faculty_id = body.faculty.id
			return res.render('CMS/faculty/show', {
				layout: 'CMS/layout/master',
				data: body,
				message: req.flash('FacultyMessage'),
			});
		}
		return res.status(404).json({'error' : error});
	})
	
}
exports.create = function(req, res)
{
	return res.render('CMS/listing/create', {
		layout: 'CMS/layout/master'
	});
};

function get_listing_byid (list, id){
	for (var i = 0;i<list.length;i++){
		id = parseInt(id)
		if (list[i].id == id){
			return list[i]
		}
	}
}
exports.edit = function(req, res)
{	
	var listing = get_listing_byid(req.session.listings , req.params.id)
	if (listing.image_id) {
		req.session.image_id_edit = listing.image_id.id;
	}
	return res.render('CMS/listing/edit', {
		layout: 'CMS/layout/master',
		listing: listing
	});
}

exports.update = function(req, res)
{
	req.assert('name', 'Name is empty').notEmpty();
  
  var errors = req.validationErrors();

  if (errors.length>0){
  	return res.status(400).json({'error-form' : errors});
  }

 
  var scholar = {
  "id" : req.params.id,
  "provider_id": req.user.user_id,
  "date_start": req.body.start_date.replace('/','').replace('/',''),
	"date_experied": req.body.end_date.replace('/','').replace('/',''),
  "scholarship_type": "2",
  "status":"0",
  "title": req.body.title,
  "quota": req.body.quota,
  "descripe": req.body.ckedit,
  "country_id": "2",
  "grade_id": "2",
  "image_id": req.session.image_id_edit,
  "city_id": "2",
  "minimum_grade": "2",
  "faculty_id": "2",
  "program_id": "2",
  "type_country": "0",
  "scholarshippoint_id": "1"
  
}
console.log(" UPDATE IMAGE LAST ID :", req.session.image_id_edit)
var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
var options = {
		method: 'POST',
		url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/scholarships/update',
		headers : {
         	"Authorization" : auth,
					'Content-Type': 'application/json',
        },
		json: scholar,
		
};
request(options, function (error, response, body) {
	console.log('checkk edit', body)	
	  if (body && body.success) {
	  	req.flash('listMessage', 'Scholarship edited')
	      return res.redirect('/partner/listings');
	  }
	  return res.status(404).json({'error-update' : error , 'response' : body});
	})

}



exports.delete = function(req, res, next){
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");

	var options = {
			method: 'DELETE',
			url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/facultys/'+req.params.id,
			headers : {
	         	"Authorization" : auth,
						'Content-Type': 'application/json',
	        },
			
	};
	request(options, function (error, response, body) {
        if (typeof(body) == 'string') {
		  		body = JSON.parse(body);;
		  	}
        if (body && body.success) {
        	req.flash('AccountMessage', 'Faculty Deleted')
            return res.redirect('/partner/faculty');
        }
        return res.status(404).json({'error' : error});
    })

}
exports.store = function(req, res, next)
{
    req.assert('title', 'Title is empty').notEmpty();
    req.assert('start_date', 'Invalid start date').notEmpty();
    req.assert('end_date', 'Invalid end date').notEmpty();
    req.assert('ckedit', 'Description is empty').notEmpty();
    req.assert('quota', 'Quota is empty').notEmpty();
    var errors = req.validationErrors();

    if (errors.length>0){
    	return res.status(400).json({'error' : errors});
    }
    var scholar = {
	  "provider_id": req.user.user_id,
	  "date_start": req.body.start_date.replace('/','').replace('/',''),
		"date_experied": req.body.end_date.replace('/','').replace('/',''),
	  "scholarship_type": "2",
	  "status":"0",
	  "title": req.body.title,
	  "quota": req.body.quota,
	  "descripe": req.body.ckedit,
	  "country_id": "2",
	  "grade_id": "2",
	  "image_id": req.listing_image_id,
	  "city_id": "2",
	  "minimum_grade": "2",
	  "faculty_id": "2",
	  "program_id": "2",
	  "type_country": "0",
	  "scholarshippoint_id": "1"
	  
	}
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
	var options = {
			method: 'POST',
			url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/scholarships/create',
			headers : {
	         	"Authorization" : auth,
						'Content-Type': 'application/json',
	        },
			json: scholar,
			
	};
	request(options, function (error, response, body) {
        if (body && body.success) {
        	req.flash('listMessage', 'Scholarship created')
            return res.redirect('/partner/listings');
        }
        return res.status(404).json({'error' : 'Not found'});
    })

    
};
