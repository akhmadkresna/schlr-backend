'use strict';
var request = require('request');
var querystring = require('querystring');
var fs = require('fs');
var FormData = require('form-data');


exports.index = function(req, res)
{		
		var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
		var headers = {
			'Authorization' : auth,
            'Content-Type': 'application/json',
            
        }
        var options = {
                url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/providers/showcandidate',
                method: 'POST',
                headers: headers,
                // json: {'provider_id': 'Org-62-1503163043.67157'}
                json: {'provider_id': req.user.user_id}

            } 
        
        request(options, function (error, response, body) {
					if (body && body.success) {
						req.session.listings = body.candidate;
							return res.render('CMS/candidate/index', {
							layout: 'CMS/layout/master',
							message: req.flash('listMessage'),
							listings: req.session.listings ,
							keyword: '',
							status: '',
							currentpage: (req.query.page === undefined?1:req.query.page),
							totalPage: Math.ceil(parseInt(req.session.listings.count)/20),
						});
					}
					return res.status(404).json({'error' : 'Not found'});
        }).on('error', function(err) {
					return res.status(500).json({'error' : err});
        });
		
	

};

exports.show = function(req, res)
{
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
	var headers = {
		'Authorization' : auth,
			'Content-Type': 'application/json',
			
	}
	var options = {
					url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/facultys/showonefaculty',
					method: 'POST',
					headers: headers,
					json: {
						'provider_id': req.user.user_id,
						'id': req.params.id
					}

			} 

	console.log("OPTIONSSS :",options)
	
	request(options, function (error, response, body) {
		console.log("student stuff ::",body)
		if (body && body.success) {
			return res.render('CMS/faculty/show', {
				layout: 'CMS/layout/master',
				data: body,
				message: req.flash('FacultyMessage'),
			});
		}
		return res.status(404).json({'error' : error});
	})
	
}
exports.create = function(req, res)
{
	return res.render('CMS/listing/create', {
		layout: 'CMS/layout/master'
	});
};

function get_listing_byid (list, id){
	for (var i = 0;i<list.length;i++){
		id = parseInt(id)
		if (list[i].id == id){
			return list[i]
		}
	}
}
exports.edit = function(req, res)
{	
	var listing = get_listing_byid(req.session.listings , req.params.id)
	if (listing.image_id) {
		req.session.image_id_edit = listing.image_id.id;
	}
	return res.render('CMS/listing/edit', {
		layout: 'CMS/layout/master',
		listing: listing
	});
}

exports.update = function(req, res)
{
	req.assert('name', 'Title is empty').notEmpty();
  var errors = req.validationErrors();

  if (errors.length>0){
  	return res.status(400).json({'error-form' : errors});
  }
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
	var options = {
			method: 'POST',
			url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/programs/update',
			headers : {
						"Authorization" : auth,
						'Content-Type': 'application/json',
					},
			json: {
				"id": req.params.id,
				"grade_id": req.body.grade_id,
				"faculty_id": req.body.faculty_id,
				"name": req.body.name
			},
			
	};
	request(options, function (error, response, body) {
			if (body && body.success) {
				req.flash('FacultyMessage', 'Program edited')
					return res.redirect('/partner/faculty');
			}
			return res.status(404).json({'error-update' : error , 'response' : body});
		})

}



exports.delete = function(req, res, next){
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");

	var options = {
			method: 'POST',
			url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/programs/delete',
			headers: {
	         	"Authorization" : auth,
						'Content-Type': 'application/json',
	        },
	    json: {
	    	'id': req.params.id
	    }
			
	};
	request(options, function (error, response, body) {
        if (typeof(body) == 'string') {
		  		body = JSON.parse(body);;
		  	}
        if (body && body.success) {
        	req.flash('FacultyMessage', 'Program Deleted')
          return res.redirect('/partner/faculty');
        }
        return res.status(404).json({'error' : error});
    })

}
exports.store = function(req, res, next)
{
    req.assert('name', 'Title is empty').notEmpty();
    
    var errors = req.validationErrors();

    if (errors.length>0){
    	return res.status(400).json({'error' : errors});
    }
    var program = {
		  'name': req.body.name,
		  'faculty_id': req.body.faculty_id,
			'grade_id': req.body.grade_id
		  
		  
		}
	console.log("Programs ::",program)
	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
	console.log("AUTH ====================",auth)
	var options = {
			method: 'POST',
			url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/programs/create',
			headers : {
	         	"Authorization" : auth,
						'Content-Type': 'application/json',
	        },
			json: program,
			
	};
	request(options, function(err, response, body) {
        if (body && body.success) {
            req.flash('FacultyMessage', 'Program Added')
            return res.redirect('/partner/faculty');
        }
        return res.status(500).json({'error' : body});

    });  

    
};
