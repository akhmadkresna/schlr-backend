'use strict';
var request = require('request');
var request_promise = require('request-promise');
var Promise = require('bluebird');

exports.faculty = function(req, res){
    
    var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
    var url = 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/providers/showfacultys'

    var headers = {
        'Authorization' : auth,
        'Content-Type': 'application/json'      
    }
    var options = {
            url: url,
            method: 'POST',
            headers: headers,
            json: {
                'provider_id': req.user.user_id
            }
        } 

    request(options, function(err, response, body) {
        if (body && body.success) {
            return res.render('CMS/account/faculty', {
                layout: 'CMS/layout/account',
                faculty: body.faculty,
                page_name: 'faculty',
                message: req.flash('AccountMessage'),
            });
        }
        return res.status(500).json({'error' : body});

    });  
    
}
exports.createfaculty = function(req, res)
{
    var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
    var url = 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/facultys'

    var headers = {
        'Authorization' : auth,
        'Content-Type': 'application/json'      
    }
    var options = {
            url: url,
            method: 'POST',
            headers: headers,
            json: {
                'setupfaculty_id': req.session.setupfaculty_id,
                'provider_id': req.user.user_id
            }
        } 

    request(options, function(err, response, body) {
        if (body && body.success) {
            req.flash('AccountMessage', 'Faculty Added')
            return res.redirect('/partner/faculty');
        }
        return res.status(500).json({'error' : body});

    });  
};

exports.setupfaculty = function(req, res, next)
{
    var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
    var url = 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/setupfacultys'

    var headers = {
        'Authorization' : auth,
        'Content-Type': 'application/json'      
    }
    var options = {
            url: url,
            method: 'POST',
            headers: headers,
            json: {
                'name': req.body.name,
            }
        } 

    request(options, function(err, response, body) {
        if (err) {
            return res.status(500).json({'error' : err});
        }
        if (typeof(body) == 'string') {
            body = JSON.parse(body);;
        }
        if (body){
            if (typeof(body.id) == 'string'){
                body.id = parseInt(body.id);
            }
            if (body.success) {
                req.session.setupfaculty_id = body.setup_faculty.id
                return next();
            }
        }
        return res.status(500).json({'error' : body});

    });  
};
exports.index = function(req, res) 
{

	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
	var headers = {
		'Authorization' : auth,
        'Content-Type': 'application/json',
        
    }
    var requests = [{
		url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/facultys/showallfaculty',
		method: 'POST',
		headers: headers,
		json: {'provider_id': req.user.user_id}
	}, {
		url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/providers/show',
		method: 'POST',
		headers: headers,
		json: {'provider_id': req.user.user_id}
	}];
    var options = {
            url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/providers/show',
            method: 'POST',
            headers: headers,
            // json: {'provider_id': 'Org-62-1503163043.67157'}
            json: {'provider_id': req.user.user_id}

        } 

    Promise.map(requests, function(obj) {
        return request_promise(obj).then(function(body) {
            return body;
        });
    }).then(function(results) {
        var faculty = []
        var provider = {}
        console.log("DOUBLE CALL account :",results);
        for (var i = 0; i < results.length; i++) {
            
            if (results[i].success && results[i].hasOwnProperty('faculty')){
                console.log("ADA FACULTY :::",results[i].faculty)
                req.session.faculty = results[i].faculty
            }
            if (results[i].success && results[i].hasOwnProperty('provider')){
                req.session.provider = results[i].provider
            }
        }
        console.log("results[i]",faculty)
        return res.render('CMS/account/index', {
            layout: 'CMS/layout/account',
            provider: req.session.provider,
            faculty: req.session.faculty,
            page_name: 'description',
            message: req.flash('AccountMessage'),
        });
    }, function(err) {
        return res.status(500).json({'error' : err});
    });
   
	
};


exports.show = function(req, res) 
{
    if (req.session.faculty != undefined ){
        return res.render('CMS/account/faculty', {
            layout: 'CMS/layout/account',
            faculty: req.session.faculty,
            page_name: 'faculty',
            message: req.flash('AccountMessage'),
        });
    }
    else {
        alert("call api")
    }
}
exports.update = function(req, res) 
{

	var auth = "Basic " + new Buffer(req.user.api_key + ":" + req.user.api_secret).toString("base64");
	var headers = {
		'Authorization' : auth,
        'Content-Type': 'application/json',
        
    }
    var options = {
            url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/providers/update',
            method: 'POST',
            headers: headers,
            // json: {'provider_id': 'Org-62-1503163043.67157'}
            json: {"provider_id": req.user.user_id,
				  "name": req.body.name,
				  "address": req.body.address,
				  "contact_number": req.body.contact_number,
				  "fax": req.body.fax,
				  "email": req.body.email,
				  "web_url": req.body.web,
				  "description": req.body.ckedit,
				  "user_id": req.user.id,
				  "country_id": "1",
				  "city_id": "1"}

        }
    request(options, function (error, response, body) {
    		console.log('Profile update',body);
        if (body && body.success) {
          req.flash('AccountMessage', 'Profile Updated')
	      return res.redirect('/partner/Profile');
        }
        return res.status(404).json({'error' : error,
        	'body' : body
      	});
    })
	
};
exports.signup = function(req, res) 
{

	
	var headers = {
       'Content-Type': 'application/json',
        
    }
    var options = {
            url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/auth/register',
            method: 'POST',
            headers: headers,
            // json: {'provider_id': 'Org-62-1503163043.67157'}
            json: {	
    				'email': req.body.email,
    				'password': req.body.password,
    				'user_type' : 1
    			}

        } 
    
    request(options, function (error, response, body) {
    	console.log("register body :",body, typeof(body))
        if (body && body.success) {
          return res.render('CMS/login', {
						layout: 'CMS/layout/singleform'
					});
        }
        return res.status(404).json({'error' : error});
    })
	
};