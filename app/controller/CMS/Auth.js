'use strict';

exports.show = function(req, res) 
{
	return res.render('CMS/login', {
		layout: 'CMS/layout/singleform'
	});
};

exports.destroy = function (req, res) 
{
	req.logout();
	res.redirect('/partner/login');
}