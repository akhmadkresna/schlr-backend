'use strict';

exports.index = function(req, res) 
{
	return res.render('WEB/index', {
		layout: 'CMS/layout/singleform',
		googlemeta: ''
		// googlemeta: (process.env.NODE_ENV == 'production')?'<meta name="google-site-verification" content="gTFox6d_5Yig1BT9pPqW-aW4aaUohd9Tuda-1wTemNI" />':''
	});
};

exports.tnc = function(req, res) 
{
	return res.render('WEB/tnc', {
		layout: 'WEB/layout/master'
	});
}