module.exports = function(app, path, express) {
	
	var expressLayouts = require('express-ejs-layouts')
	app.set('view engine', 'ejs');

	app.set('views', global.appRoot + global.APP_CONFIGS.app.views);
	app.use(expressLayouts);

	app.use(express.static(path.join(global.appRoot, 'resource/public')));

}