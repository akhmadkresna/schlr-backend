module.exports = function(passport, app) {

    var LocalStrategy   = require('passport-local').Strategy;
    var request = require('request');

    passport.serializeUser(function(user, done) {
        // console.log("CEK USER :")
        done(null, user);
    });

    passport.deserializeUser(function(admin, done) {
        if (admin && admin.user_type == '1'){
            done(null, admin);
        } else {
            done('error', null);
        }
    });

    passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req, email, password, done) {
        var headers = {
            'Content-Type': 'application/json'
        }
        // console.log("CONFIG REST ::",global.APP_CONFIGS.app.rest_backend)
        var options = {
                url: 'https://fast-eyrie-73551.herokuapp.com/devschoolarapi/v1/auth/login',
                method: 'POST',
                headers: headers,
                json: {'email': email, 'password': password}
            } 
        request(options, function (error, response, body) {
            
            if (body && body.success) {
                return done(null, body.user);
            }
            return done(error);
        }).on('error', function(err) {
            return done(err);
        });

    }));

    app.use(passport.initialize());
    app.use(passport.session());
};
