module.exports = function(app, expressValidator) {

	var expressValidator = require('express-validator');
	app.use(expressValidator());

	var customValidators = {
		validPassword: function(value){
			return (value.length < 8)?false:(value.match(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/) != null);
		},
		isSlug: function (value) {
			return (value.length < 2)?false:(value.match(/^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/) != null);
		},
		notEqual: function (value, value2) {
			return (value!=value2)
		},
		arrayInt: function (value, min) {
			var valid = true;
			for (var i = value.length - 1; i >= 0; i--) 
			{
				if (isNaN(value[i]) || value[i]<min) 
				{
					valid = false;
					break;
				}
			}
			return valid;
		}
	}
	
	app.use(expressValidator({
		customValidators: customValidators
	}));


}